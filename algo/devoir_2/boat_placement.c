#include "guillerot.h"

bool	test_boat_position(char board[BOARD_SIZE][BOARD_SIZE], int boat_size, int x, int y, bool vertical) {
	/*
	** Test if the boat can correctly be place in the board without overlapping
	** others boats.
	**
	** Args:
	**	board: game board array
	**	boat_size: length of the boat to place
	**	x: x coordinate
	**	y: y coordinate
	**	vertical: true = vertical position / false = horizontal position
	*/
	if (vertical) {
		if (y + boat_size >= BOARD_SIZE || y < 0)
			return (false);
		for (int i = 0; i < boat_size; ++i)
			if (board[y + i][x] == HEALTHY_BOAT)
				return (false);
	} else {
		if (x + boat_size >= BOARD_SIZE || x < 0)
			return (false);
		for (int i = 0; i < boat_size; ++i)
			if (board[y][x + i] == HEALTHY_BOAT)
				return (false);
	}
	return (true);
}

void	place_boat(char board[BOARD_SIZE][BOARD_SIZE], int boat_size, int x, int y, bool vertical) {
	/*
	** Places a boat in the computer board.
	** If the given position parameters are not correct, it tries with others
	** random parameters.
	**
	** Args:
	**	board: game board array
	**	boat_size: length of the boat to place
	**	x: x coordinate
	**	y: y coordinate
	**	vertical: true = vertical position / false = horizontal position
	*/
	if (!test_boat_position(board, boat_size, x, y, vertical))
		place_boat(
			board,
			boat_size,
			rand() % BOARD_SIZE,
			rand() % BOARD_SIZE,
			rand() % 2
		);
	else {
		for (int i = 0; i < boat_size; ++i) {
			if (vertical)
				board[y + i][x] = HEALTHY_BOAT;
			else
				board[y][x + i] = HEALTHY_BOAT;
		}
	}
}

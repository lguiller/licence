#include "guillerot.h"

void	hide_boat(char c, bool display_missed_shot) {
	/*
	** Displays a character of the board.
	** It doesn't display the position of healthy boats
	**
	** Args:
	**	c: character to display
	**	display_missed_shot: parameter used to print or not missed shots
	*/
	c = (c == MISSED_SHOT && !display_missed_shot) ? EMPTY : c;
	printf("%c ", c == HEALTHY_BOAT ? EMPTY : c);
}

void	visible_boat(char c, bool display_missed_shot) {
	/*
	** Displays a character of the board.
	** It displays the position of healthy boats
	**
	** Args:
	**	c: character to display
	**	display_missed_shot: not used in this function
	*/
	c = (c == MISSED_SHOT && !display_missed_shot) ? EMPTY : c;
	printf("%c ", c);
}

void	print_board(t_game game, void (*f)(char, bool)) {
	/*
	** Display the entire board.
	**
	** Args:
	**	board: game board array
	**	f: function pointer for choosing between display boats or not
	*/
	system("clear");
	printf("%s", game.debug ? "Debug mode ON\n\n" : "");
	printf("  0 1 2 3 4 5 6 7 8 9\n");
	for (int y = 0; y < BOARD_SIZE; ++y) {
		printf("%c ", (char)('A' + y));
		for (int x = 0; x < BOARD_SIZE; ++x)
			f(game.board[y][x], game.display_missed_shot);
		printf("\n");
	}
	printf("\n");
}

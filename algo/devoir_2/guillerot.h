#ifndef GUILLEROT_H
# define GUILLEROT_H

# include <stdlib.h>
# include <stdio.h>
# include <stdbool.h>
# include <time.h>

# define BOARD_SIZE			10
# define TYPE_NUMBER		4

# define EMPTY				' '
# define HEALTHY_BOAT		'#'
# define DAMAGED_BOAT		'X'
# define MISSED_SHOT		'.'

# define MENU_KEY			'M'

# define DEBUG_MODE			false

# define WRONG_INPUT_MSG	"Error: Wrong input.\n"

# define UPPER_CASE(c)		(c >= 'a' && c <= 'z' ? c - ('a' - 'A') : c)
# define LETTER_TO_INT(c)	(c - 'A')
# define NUMBER_TO_INT(c)	(c - '0')

typedef enum	e_main_menu_option {
	NEW_GAME = '1',
	SETTINGS,
	SURRENDER,
	EXIT_MAIN_MENU,
	QUIT,
}				t_main_menu_option;

typedef enum	e_settings_menu_option {
	DISPLAY_OCEAN_HIT = '1',
	DEBUG,
	EXIT_SETTINGS_MENU,
}				t_settings_menu_option;

typedef struct	s_game {
	char		board[BOARD_SIZE][BOARD_SIZE];
	int			number_of_strokes;
	bool		display_missed_shot;
	bool		started;
	bool		debug;
}				t_game;

void			hide_boat(char c, bool display_missed_shot);
void			visible_boat(char c, bool display_missed_shot);
void			print_board(t_game game, void (*f)(char, bool));
void			place_boat(char board[BOARD_SIZE][BOARD_SIZE], int boat_size, int x, int y, bool vertical);
void			init_game(t_game *game);
void			get_player_input(char input[3]);
int				test_win(char board[BOARD_SIZE][BOARD_SIZE]);
void			main_menu(t_game *game);

#endif

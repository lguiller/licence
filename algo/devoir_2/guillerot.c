/*
** Auteur: GUILLEROT Lucas
**
** Ce qui fonctionne:
** Normalement tout, sauf le bonus.
**
** Ce qui ne fonctionne pas:
** Normalement Rien.
*/

#include "guillerot.h"

void	execute_action(t_game *game, int x, int y) {
	/*
	** Update computer's board by the right character.
	**
	** Args:
	**	game: pointer on the game data structure
	**	x: strike coordinate x
	**	y: strike coordinate y
	*/
	switch (game->board[y][x]) {
		case EMPTY:
			game->board[y][x] = MISSED_SHOT;
			break;
		case HEALTHY_BOAT:
			game->board[y][x] = DAMAGED_BOAT;
			break;
	}
	++game->number_of_strokes;
}

int		test_win(char board[BOARD_SIZE][BOARD_SIZE]) {
	/*
	** Test if the player has won by counting computer's healthy boats.
	**
	** Args:
	**	board: board array
	**
	** Return:
	**	true if the player won else return false
	*/
	for (int y = 0; y < BOARD_SIZE; ++y)
		for (int x = 0; x < BOARD_SIZE; ++x)
			if (board[y][x] == HEALTHY_BOAT)
				return (false);
	return (true);
}

void	start_game(t_game game) {
	/*
	** Game loop.
	**
	** Args:
	**	game: game data structure
	*/
	bool	running = true;
	char	input[3] = {0};

	while (running) {
		if (!game.started) {
			main_menu(&game);
			continue;
		}
		print_board(game, game.debug ? visible_boat : hide_boat);

		get_player_input(input);

		if (input[0] == MENU_KEY) {
			main_menu(&game);
			continue;
		}

		execute_action(
			&game,
			NUMBER_TO_INT(input[1]),
			LETTER_TO_INT(input[0])
		);

		if (test_win(game.board)) {
			print_board(game, game.debug ? visible_boat : hide_boat);
			printf("Well played, you won in %d tries !\n", game.number_of_strokes);
			printf("Press enter to continue...\n");
			getchar();
			game.started = false;
		}
	}
}

int		main(void) {
	t_game	game;

	srand(time(NULL));

	game.debug = DEBUG_MODE;
	init_game(&game);
	start_game(game);
	return (0);
}

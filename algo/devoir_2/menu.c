#include "guillerot.h"

void	clear_stdin(void) {
	/*
	** Clear standar input.
	*/
	while (getchar() != '\n');
}

void	get_player_input(char input[3]) {
	/*
	** Game prompt.
	**
	** Args:
	**	input[3]: array of 3 character to return user input
	*/
	while (true) {
		printf("Type 'M' for Menu.\n");
		printf("Type a coordinate to play (ex: F4).\n");
		printf("> ");
		scanf("%2s", input);
		clear_stdin();

		input[0] = UPPER_CASE(input[0]);
		if ((input[0] >= 'A' && input[0] <= 'J'
			&& input[1] >= '0' && input[1] <= '9')
			|| (input[0] == MENU_KEY && input[1] == 0)) {
			break;
		}
		printf(WRONG_INPUT_MSG);
	}
}

bool	ask_for_display_missed_shot(void) {
	/*
	** Ask the user if missed shots should be printed or not.
	**
	** Return:
	**	User input character
	*/
	char	response;

	system("clear");
	while (true) {
		printf("Faire apparaitre les tirs rate ? (O/N)\n");
		printf("> ");
		response = getchar();
		clear_stdin();
		response = UPPER_CASE(response);
		switch (response) {
			case 'O':
				return (true);
			case 'N':
				return (false);
			default:
				system("clear");
				printf(WRONG_INPUT_MSG);
				break;
		}
	}
}

void	settings_menu(t_game *game) {
	/*
	** Print settings menu with its prompt.
	**
	** Args:
	**	game: pointer on the game data structure
	*/
	char	answer;
	bool	menu_on = true;

	while (menu_on) {
		system("clear");
		printf("%s", game->debug ? "Debug mode ON\n\n" : "");
		printf(" ======================== \n");
		printf("|        Settings        |\n");
		printf("|------------------------|\n");
		printf("|show missed shot (%d)   %c|\n", game->display_missed_shot, DISPLAY_OCEAN_HIT);
		if (DEBUG_MODE)
			printf("|Debug mode (%d)         %c|\n", game->debug, DEBUG);
		printf("|Back                   %c|\n", EXIT_SETTINGS_MENU);
		printf(" ======================== \n");
		printf("> ");
		scanf("%c", &answer);
		clear_stdin();

		switch (answer) {
			case DISPLAY_OCEAN_HIT:
				game->display_missed_shot = !game->display_missed_shot;
				break;
			case DEBUG:
				game->debug = DEBUG_MODE ? !game->debug : false;
				break;
			case EXIT_SETTINGS_MENU:
				menu_on = false;
				break;
		}
	}
}

void	main_menu(t_game *game) {
	/*
	** Print main menu with its prompt.
	**
	** Args:
	**	game: pointer on the game data structure
	*/
	char	answer;
	bool	menu_on = true;

	while (menu_on) {
		system("clear");
		printf("%s", game->debug ? "Debug mode ON\n\n" : "");
		printf(" ======================== \n");
		printf("|       Battleship       |\n");
		printf("|------------------------|\n");
		printf("|New game               %c|\n", NEW_GAME);
		printf("|Settings               %c|\n", SETTINGS);
		printf("|Surrender              %c|\n", SURRENDER);
		printf("|Back                   %c|\n", EXIT_MAIN_MENU);
		printf("|Quit                   %c|\n", QUIT);
		printf(" ======================== \n");
		printf("> ");
		scanf("%c", &answer);
		clear_stdin();

		answer = UPPER_CASE(answer);
		switch (answer) {
			case NEW_GAME:
				init_game(game);
				game->display_missed_shot = ask_for_display_missed_shot();
				game->started = true;
				menu_on = false;
				break;
			case SETTINGS:
				settings_menu(game);
				break;
			case SURRENDER:
				if (game->started) {
					printf("Game Over, you've surrendered.\n");
					printf("Press enter to continue...\n");
					getchar();
				}
				game->started = false;
				break;
			case EXIT_MAIN_MENU:
				menu_on = false;
				break;
			case QUIT:
				exit(0);
				break;
		}
	}
}

#include "guillerot.h"

void	init_board(char board[BOARD_SIZE][BOARD_SIZE]) {
	/*
	** Initialize the board by randomly placing all boats.
	**
	** Args:
	**	board: game board array
	*/
	const int	boat_number = 0;
	const int	boat_size = 1;
	const int	boat_list[TYPE_NUMBER][2] = {{1, 5}, {1, 4}, {3, 3}, {4, 2}};

	for (int y = 0; y < BOARD_SIZE; ++y) {
		for (int x = 0; x < BOARD_SIZE; ++x) {
			board[y][x] = EMPTY;
		}
	}
	for (int type = 0; type < TYPE_NUMBER; ++type)
		for (int i = 0; i < boat_list[type][boat_number]; ++i)
			place_boat(
				board,
				boat_list[type][boat_size],
				rand() % BOARD_SIZE,
				rand() % BOARD_SIZE,
				rand() % 2
			);
}

void	init_game(t_game *game) {
	/*
	** initialize the game data.
	**
	** Args:
	**	game: pointer on the game data structure
	*/
	init_board(game->board);
	game->number_of_strokes = 0;
	game->display_missed_shot = true;
	game->started = false;
}

/* Guillerot Lucas */


/* Exercice 1 Question 1 */
//@ predicate cte(int a[], int c) = forall i. 0 <= i < length(a) -> a[i] == c;

/* Exercice 1 Question 2 */
//@ predicate no_dup(int a[]) = forall i, j. 0 <= i < j < length(a) -> a[i] != a[j];

/* Exercice 1 Question 3 */
//@ lemma cte_no_dup: forall a[]. length(a) >= 2 -> forall c. cte(a, c) -> !no_dup(a);

/* Exercice 1 Question 4 */
//@ lemma cte_2: forall t[]. length(t) >= 2 -> forall c. cte(t, c) -> t[0] == t[1];
//@ lemma cte_no_dup_2: forall t[]. length(t) >= 2 -> t[0] == t[1] -> !no_dup(t);

/* Exercice 1 Question 5 */
// Les lemmes cte_no_dup_2 et cte_2 doivent être placés avant le lemme cte_no_dup pour aider la démonstration du lemme de la question 3.


/* Exercice 2 Question 1 */
int carre(int x) {
	/* Exercice 2 Question 2 */
	//@requires x >= 0;
	//@ensures result == x * x;

	int i = 0;
	int c = 0;
	while (i < x) {
		/* Exercice 2 Question 3 */
		//@invariant 0 <= i <= x;
		//@invariant c == i * i;
		//@variant x - i;

		c = c + 2 * i + 1;
		i = i + 1;
	}
	return c;
}


/* Exercice 3 Question 1 */
int max(int t[], int n) {
	/* Exercice 3 Question 2 */
	//@requires n > 0;
	//@requires length(t) == n;
	//@ensures forall i. 0 <= i < n -> result >= t[i];
	//@ensures exists i. 0 <= i < n && t[i] == result;

	int m = t[0];
	for (int i = 1; i < n; i++) {
		/* Exercice 3 Question 3 */
		//@invariant forall j. 0 <= j < i -> m >= t[j];
		//@invariant exists j. 0 <= j < i && t[j] == m;
		//@invariant 0 <= i <= n;
		//@variant n - i;

		if (t[i] > m) {
			m = t[i];
		}
	}
	return m;
}


/* Exercice 4 Question 1 */
int recherche_dychotomique(int t[], int n, int x) {
	/* Exercice 4 Question 2 */
	//@requires n >= 0;
	//@requires length(t) == n;
	//@requires forall i, j. 0 <= i < j < n -> t[i] <= t[j];
	//@ensures forall k. 0 <= k < result -> t[k] <= x;
	//@ensures forall k. result <= k < n -> t[k] > x;

	int min = 0;
	int max = n;

	while (min != max) {
		/* Exercice 4 Question 3 */
		//@invariant 0 <= min <= max <= n;
		//@invariant forall k. 0 <= k < min -> t[k] <= x;
		//@invariant forall k. max <= k < n -> t[k] > x;
		//@variant max - min;

		int m = (min + max) / 2;

		if (t[m] <= x) {
			min = m + 1;
		} else {
			max = m;
		}
	}
	return min;
}

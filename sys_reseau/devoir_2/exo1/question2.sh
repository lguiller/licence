#!/bin/bash

printf "Entrez un nom de répertoire: "
read REPO_NAME
REPO_PATH="/home/$REPO_NAME"

if [ -n "$REPO_NAME" ]; then
	if [ -e "$REPO_PATH" ]; then
		if [ -r "$REPO_PATH" ]; then
			cd "$REPO_PATH"
		else
			echo "Error: Accès refusé"
			cd $HOME
		fi
	else
		echo "Error: Directory does not exist"
		cd $HOME
	fi
else
	cd $HOME
fi

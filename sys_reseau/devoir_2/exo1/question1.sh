#!/bin/bash

# CREATION DES GROUPES ET DE LEUR HOME AVEC LES BONS DROITS D'ACCES
for group in licence3 reinscription enseignement accesSSH; do
	groupadd $group && install -d -m 070 -g $group /home/$group;
done

# CREATION DES UTILISATEURS, DE LEUR GROUPE PRINCIPALE ET DE LEUR HOME DIRECTORY
for user in alice bob charlie eve; do useradd -m -g accesSSH -s /bin/bash $user; done

# AJOUT DES UTILISATEURS DANS LEUR GROUPES SECONDAIRES
for user in alice charlie; do usermod -a -G licence3 $user; done
for user in alice bob; do usermod -a -G reinscription $user; done
for user in eve; do usermod -a -G enseignement $user; done

# ATTRIBUTION DES DROITS D'ACCES POUR LES HOME DES UTILISATEURS
for user in alice bob charlie eve; do chmod 700 /home/$user; done

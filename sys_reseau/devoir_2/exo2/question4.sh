#!/bin/bash

ps -ax -o pid,ni --sort=+ni --no-headers | sed -n '1p' | awk '{print $1,$2}'

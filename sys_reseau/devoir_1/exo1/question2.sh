#!/bin/bash

echo "1:"
COMMAND="echo -e \"C'est un petit fichier\n\
qui contient le mot soleil\n\
en plusieurs exemplaires\n\
pour effectuer des recherches\n\
sur le mot soleil\" > ~/Devoir1/Exercice1/text1.tex"
echo $COMMAND ; eval $COMMAND
echo

echo "2:"
COMMAND="cp ~/Devoir1/Exercice1/text1.tex ~/Devoir1/Exercice1/1-text;cp ~/Devoir1/Exercice1/text1.tex ~/Devoir1/Exercice1/2-text.tex"
echo $COMMAND ; eval $COMMAND
echo

echo "3:"
COMMAND="touch ~/Devoir1/test1.core;touch ~/Devoir1/Exercice1/test2.core"
echo $COMMAND ; eval $COMMAND
echo

echo "4:"
COMMAND="grep \"soleil\" ~/Devoir1/Exercice1/text1.tex"
echo $COMMAND ; eval $COMMAND
echo

echo "5:"
COMMAND="grep \"soleil\" *.tex"
echo $COMMAND ; eval $COMMAND
echo

echo "6:"
COMMAND="find ~/ -user \`whoami\` -name \"*.core\" -print"
echo $COMMAND ; eval $COMMAND
echo

echo "7:"
COMMAND="find ~/ -user \`whoami\` -name \"*.core\" -exec rm {} \\;"
echo $COMMAND ; eval $COMMAND
echo

echo "8:"
COMMAND="find ~/Devoir1/Exercice1 -not -name \"[a-zA-Z]*\" -print"
echo $COMMAND ; eval $COMMAND
echo

echo "9:"
COMMAND="find ~/Devoir1/Exercice1 -not -name \"[a-zA-Z]*\" -exec cat {} \;"
echo $COMMAND ; eval $COMMAND
echo

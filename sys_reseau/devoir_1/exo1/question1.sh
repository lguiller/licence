#!/bin/bash

echo "1:"
COMMAND="mkdir -p ~/Devoir1/Exercice1"
echo $COMMAND ; eval $COMMAND
echo

echo "2:"
COMMAND="cat /etc/passwd | cut -c-3"
echo $COMMAND ; eval $COMMAND
echo

echo "3:"
COMMAND="cut -d ':' -f 5 /etc/passwd | head -10"
echo $COMMAND ; eval $COMMAND
echo

echo "4:"
COMMAND="cat /etc/passwd | wc -l"
echo $COMMAND ; eval $COMMAND
echo

echo "5:"
COMMAND="cut -d ':' -f 5 /etc/passwd | sort > ~/Devoir1/Exercice1/utilisateur"
echo $COMMAND ; eval $COMMAND
echo


#include "maze.h"
#include <stdio.h>
using namespace maze;

Maze::Maze(const char ascii[16][16], startPoint point) {
	// créer les noeuds en fonction du caractère
	for (int i = 0; i < 16; ++i) {
		for (int j = 0; j < 16; ++j) {
			switch (ascii[i][j]) {
				case 'w':
					this->nodes[i][j] = new mazeNode::NodeWall();
					break;
				case ' ':
					this->nodes[i][j] = new mazeNode::NodeFree();
					break;
				case 'c':
					this->nodes[i][j] = new mazeNode::NodeCheese();
					break;
				default:
					this->nodes[i][j] = nullptr;
					break;
			}
		}
	}

	// créer les liens entres voisins
	for (int i = 0; i < 16; ++i) {
		for (int j = 0; j < 16; ++j) {
			if (i > 0 && this->nodes[i][j] != nullptr && this->nodes[i - 1][j] != nullptr) {
				this->nodes[i][j]->setNeighbor(mazeNode::NodeNeighbor_Up, this->nodes[i - 1][j]);
				this->nodes[i - 1][j]->setNeighbor(mazeNode::NodeNeighbor_Down, this->nodes[i][j]);
			}
			if (j > 0 && this->nodes[i][j] != nullptr && this->nodes[i][j - 1] != nullptr) {
				this->nodes[i][j]->setNeighbor(mazeNode::NodeNeighbor_Left, this->nodes[i][j - 1]);
				this->nodes[i][j - 1]->setNeighbor(mazeNode::NodeNeighbor_Right, this->nodes[i][j]);
			}
		}
	}

	// point de départ
	this->starter = (
		this->nodes[point.line][point.col]->isFreeToVisit()
		? this->nodes[point.line][point.col]
		: nullptr
	);
}

Maze::~Maze() {
	// peut-être des choses à supprimer
	for (int i = 0; i < 16; ++i)
		for (int j = 0; j < 16; ++j)
			delete this->nodes[i][j];
}

bool Maze::checkConsistency() {
	// tous les noeuds sont instanciés
	for (int i = 0; i < 16; ++i)
		for (int j = 0; j < 16; ++j)
			if (this->nodes[i][j] == nullptr)
				return false;

	// point de départ valide
	if (this->starter == nullptr)
		return false;

	return true;
}

bool Maze::solve() {
	return this->parse(this->starter);
}

void Maze::getAsciiFormat(char ascii[16][16]) {
	for (int i = 0; i < 16; ++i)
		for (int j = 0; j < 16; ++j)
			ascii[i][j] = (
				this->nodes[i][j] == nullptr
				? 'E'
				: this->nodes[i][j]->getString()
			);
}

bool Maze::parse(mazeNode::Node *node) {
	// la récursivité peut-être intéressante pour le parcours d'un arbre de noeuds
	if (node == nullptr)
		return false;
	node->setVisited();
	if (node->getType() == mazeNode::NodeType_Cheese)
		return true;
	return ((node->isNeighborFree(mazeNode::NodeNeighbor_Up)
				&& !node->getNeighbor(mazeNode::NodeNeighbor_Up)->isVisited()
				&& this->parse(node->getNeighbor(mazeNode::NodeNeighbor_Up)))
			|| (node->isNeighborFree(mazeNode::NodeNeighbor_Right)
				&& !node->getNeighbor(mazeNode::NodeNeighbor_Right)->isVisited()
				&& this->parse(node->getNeighbor(mazeNode::NodeNeighbor_Right)))
			|| (node->isNeighborFree(mazeNode::NodeNeighbor_Down)
				&& !node->getNeighbor(mazeNode::NodeNeighbor_Down)->isVisited()
				&& this->parse(node->getNeighbor(mazeNode::NodeNeighbor_Down)))
			|| (node->isNeighborFree(mazeNode::NodeNeighbor_Left)
				&& !node->getNeighbor(mazeNode::NodeNeighbor_Left)->isVisited()
				&& this->parse(node->getNeighbor(mazeNode::NodeNeighbor_Left))));
}

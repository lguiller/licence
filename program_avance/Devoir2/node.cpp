
#include "node.h"
using namespace mazeNode;

Node::Node() {
	// par défaut les voisins sont nulls et le type du noeud NodeType_Wall
	this->type = NodeType_Wall;
	this->visited = false;
	this->up = nullptr;
	this->down = nullptr;
	this->left = nullptr;
	this->right = nullptr;
}

Node::~Node() {
}

eNodeType Node::getType() {
	// retourne le type de noeud
	return this->type;
}

void Node::setNeighbor(eNodeNeighbor position, Node* neighbor) {
	switch (position) {
		case NodeNeighbor_Up:
			this->up = neighbor;
			break;
		case NodeNeighbor_Down:
			this->down = neighbor;
			break;
		case NodeNeighbor_Left:
			this->left = neighbor;
			break;
		case NodeNeighbor_Right:
			this->right = neighbor;
			break;
	}
}

Node* Node::getNeighbor(eNodeNeighbor position) {
	switch (position) {
		case NodeNeighbor_Up:
			return this->up;
		case NodeNeighbor_Down:
			return this->down;
		case NodeNeighbor_Left:
			return this->left;
		case NodeNeighbor_Right:
			return this->right;
		default:
			return nullptr;
	}
}

bool Node::isNeighborFree(eNodeNeighbor position) {
	// Est-ce que le voisin à la position donnée existe et peut-être visité ?
	return this->getNeighbor(position) != nullptr && this->getNeighbor(position)->isFreeToVisit();
}

void Node::setVisited() {
	this->visited = true;
}

bool Node::isVisited() {
	return this->visited;
}

bool Node::isFreeToVisit() {
	return this->type == NodeType_Free || this->type == NodeType_Cheese;
}

NodeWall::NodeWall() : Node() {
	this->type = NodeType_Wall;
}

const char NodeWall::getString() {
	return 'w';
}

NodeFree::NodeFree() : Node() {
	this->type = NodeType_Free;
}

const char NodeFree::getString() {
	if (this->visited)
		return '.';
	return ' ';
}

NodeCheese::NodeCheese() : Node() {
	this->type = NodeType_Cheese;
}

const char NodeCheese::getString() {
	if (this->visited)
		return 'X';
	return 'c';
}

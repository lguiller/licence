#include "gtest/gtest.h"

#define private public

#include "connect_four.hpp"

class GameBoardClassTest: public testing::Test {
public:
	Player	*player1;
	Player	*player2;

	void SetUp() {
		player1 = new Player(PlayerType::PLAYER1);
		player2 = new Player(PlayerType::PLAYER2);
	}

	void TearDown() {
		delete player1;
		delete player2;
	}
};

TEST(PlayerClassTest, GetPlayerTypeTest) {
	const Player	player1(PlayerType::PLAYER1);
	const Player	player2(PlayerType::PLAYER2);

	EXPECT_EQ(player1.number, PlayerType::PLAYER1);
	EXPECT_STREQ(player1.name.c_str(), "Player 1");
	EXPECT_STREQ(player1.coin.c_str(), std::string(YELLOW + std::string("0") + RESET).c_str());
	EXPECT_EQ(player2.number, PlayerType::PLAYER2);
	EXPECT_STREQ(player2.name.c_str(), "Player 2");
	EXPECT_STREQ(player2.coin.c_str(), std::string(RED + std::string("0") + RESET).c_str());
}

TEST_F(GameBoardClassTest, ConstructorTest) {
	GameBoard	connect_four;

	for (int y = 0; y < GameBoard::m_height; y++) {
		for (int x = 0; x < GameBoard::m_width; x++) {
			EXPECT_EQ(connect_four.m_board[y][x], '0');
		}
	}
}

TEST_F(GameBoardClassTest, isColumnFullTest) {
	GameBoard	connect_four;

	for (int y = 0; y < GameBoard::m_height; y += 2) {
		player1->dropCoin(connect_four, 0);
		player2->dropCoin(connect_four, 0);
	}
	EXPECT_EQ(connect_four.isColumnFull(0), true);
	EXPECT_EQ(connect_four.isColumnFull(1), false);
}

TEST_F(GameBoardClassTest, DropCoinTest) {
	GameBoard	connect_four;

	player1->dropCoin(connect_four, 0);
	EXPECT_EQ(connect_four.m_board[5][0], '1');
	player2->dropCoin(connect_four, 0);
	EXPECT_EQ(connect_four.m_board[4][0], '2');
	player1->dropCoin(connect_four, 0);
	EXPECT_EQ(connect_four.m_board[3][0], '1');
	player2->dropCoin(connect_four, 0);
	EXPECT_EQ(connect_four.m_board[2][0], '2');
	player1->dropCoin(connect_four, 0);
	EXPECT_EQ(connect_four.m_board[1][0], '1');
	player2->dropCoin(connect_four, 0);
	EXPECT_EQ(connect_four.m_board[0][0], '2');
}

TEST_F(GameBoardClassTest, CheckPlayer1WinTest) {
	GameBoard	connect_four;

	player1->dropCoin(connect_four, 0);
	player2->dropCoin(connect_four, 1);
	player1->dropCoin(connect_four, 0);
	player2->dropCoin(connect_four, 1);
	player1->dropCoin(connect_four, 0);
	player2->dropCoin(connect_four, 1);
	player1->dropCoin(connect_four, 0);
	EXPECT_EQ(connect_four.checkWin(*player1), true);
	EXPECT_EQ(connect_four.checkWin(*player2), false);
}

TEST_F(GameBoardClassTest, CheckPlayer2WinTest) {
	GameBoard	connect_four;

	player1->dropCoin(connect_four, 0);
	player2->dropCoin(connect_four, 1);
	player1->dropCoin(connect_four, 0);
	player2->dropCoin(connect_four, 1);
	player1->dropCoin(connect_four, 0);
	player2->dropCoin(connect_four, 1);
	player2->dropCoin(connect_four, 1);
	EXPECT_EQ(connect_four.checkWin(*player1), false);
	EXPECT_EQ(connect_four.checkWin(*player2), true);
}

TEST_F(GameBoardClassTest, CheckDrawTest) {
	GameBoard	connect_four;

	for (int x = 0; x < GameBoard::m_width; ++x) {
		for (int y = 0; y < GameBoard::m_height; y += 2) {
			EXPECT_EQ(connect_four.checkDraw(), false);
			player1->dropCoin(connect_four, x);
			player2->dropCoin(connect_four, x);
		}
	}
	EXPECT_EQ(connect_four.checkDraw(), true);
}

TEST_F(GameBoardClassTest, CheckClear) {
	GameBoard	connect_four;

	player1->dropCoin(connect_four, 0);
	player2->dropCoin(connect_four, 1);
	player1->dropCoin(connect_four, 0);
	player2->dropCoin(connect_four, 1);
	player1->dropCoin(connect_four, 0);
	player2->dropCoin(connect_four, 1);
	player1->dropCoin(connect_four, 0);
	connect_four.clear();
	for (int y = 0; y < GameBoard::m_height; y++) {
		for (int x = 0; x < GameBoard::m_width; x++) {
			EXPECT_EQ(connect_four.m_board[y][x], '0');
		}
	}
}

int		runTests(void) {
	::testing::InitGoogleTest();
	return RUN_ALL_TESTS();
}

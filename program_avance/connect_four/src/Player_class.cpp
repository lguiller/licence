#include "Player_class.hpp"

Player::Player(PlayerType number):
	number(number),
	name("Player " + std::string(1, number)),
	coin((number == PlayerType::PLAYER1 ? YELLOW : RED) + std::string("0") + RESET) {
	/*
	 * Constructor for the Player class
	 *
	 * @param number: the number of the player
	 * @param name: the name of the player
	 * @param coin: the coin that represents the player
	 */
}

bool	Player::dropCoin(GameBoard &board, const int column) const {
	/*
	 * Drop the player's coin into the specified column
	 *
	 * @param board: the game board
	 * @param column: the column to drop the coin into
	 * @return: true if the coin was dropped, false if the column is full
	 */
	// Check if the column is full
	if (board.isColumnFull(column))
		return false;
	// Drop the coin
	board.addPlayerCoin(*this, column);
	return true;
}

#include "connect_four.hpp"

void	displayMenu(void) {
	/* Clear the terminal and display the main menu */
	system("clear");
	std::cout << "=======================" << std::endl;
	std::cout << "|      Main Menu      |" << std::endl;
	std::cout << "=======================" << std::endl;
	std::cout << "| Play              1 |" << std::endl;
	std::cout << "| Run tests         2 |" << std::endl;
	std::cout << "| Exit              3 |" << std::endl;
	std::cout << "=======================" << std::endl;
}

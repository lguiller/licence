#include "connect_four.hpp"

void	processPlayerTurn(const Player player, GameBoard &board) {
	/*
	 * This function is responsible for processing a player's turn.
	 * It will prompt the user for input and drop the player's coin
	 * into the board. It will continue to prompt the user for input
	 * until a valid move is made.
	 *
	 * @param player: The player whose turn it is.
	 * @param board: The game board.
	 */
	int		userInput;

	std::cout << std::endl << player.coin << " " << player.name << "'s turn (1-7)";
	do {
		userInput = getUserInput();
	} while (userInput < 1 || userInput > 7 || !player.dropCoin(board, --userInput));
}

void	play(const Player player1, const Player player2, GameBoard &board) {
	/*
	 * This function is responsible for running the game loop.
	 * It will continue to prompt the players for input and update
	 * the game board until a win or draw condition is met.
	 *
	 * @param player1: The first player.
	 * @param player2: The second player.
	 * @param board: The game board.
	 */
	int		playerTurn = 0;
	Player	playerList[2] = {player1, player2};

	do {
		board.print();
		processPlayerTurn(playerList[playerTurn], board);
		if (board.checkWin(playerList[playerTurn])) {
			board.print();
			std::cout << std::endl;
			std::cout << playerList[playerTurn].coin << " ";
			std::cout << playerList[playerTurn].name << " wins!" << std::endl;
			break;
		}
		if (board.checkDraw()) {
			board.print();
			std::cout << std::endl;
			std::cout << "It's a draw!" << std::endl;
			break;
		}
		playerTurn = 1 - playerTurn;
	} while (true);
	std::cout << "Press enter to continue...";
	while (getUserInput() != ESCAPE);
}

int		main(void) {
	/* 
	 * This is the main function of the program. It will display the
	 * main menu and prompt the user for input. It will then call the
	 * appropriate function based on the user's input.
	 */
	GameBoard		board;
	const Player	player1(PlayerType::PLAYER1);
	const Player	player2(PlayerType::PLAYER2);
	int				userInput;

	do {
		displayMenu();
		userInput = getUserInput();
		switch (userInput) {
			case 1:
				board.clear();
				play(player1, player2, board);
				break;
			case 2:
				runTests();
				std::cout << std::endl << "Press enter to continue...";
				while (getUserInput() != ESCAPE);
				break;
			case 3:
				return 0;
		}
	} while (true);
	return 0;
}

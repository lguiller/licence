#include <iostream>

int		getUserInput(void) {
	/*
	 * This function is used to get the user input from the keyboard
	 * without the need to press the 'enter' key. It is used to get the
	 * user input for the game
	 *
	 * @return: the user input as an integer
	 */
	int	userInput;

	system("stty raw -echo");
	userInput = getchar();
	system("stty cooked");
	// 3 is the ASCII code for the 'ctrl-c' character
	if (userInput == 3)
		exit(0);
	return userInput - '0';
}

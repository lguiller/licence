#include "Game_board_class.hpp"

GameBoard::GameBoard(void) {
	/*
	 * Default constructor
	 */
	this->m_board = new char*[this->m_height];
	for (int y = 0; y < this->m_height; y++) {
		this->m_board[y] = new char[this->m_width];
		for (int x = 0; x < this->m_width; x++)
			this->m_board[y][x] = '0';
	}
}

GameBoard::~GameBoard(void) {
	/*
	 * Destructor
	 */
	for (int y = 0; y < this->m_height; y++)
		delete []this->m_board[y];
	delete []this->m_board;
}

void	GameBoard::print(void) const {
	/*
	 * Print the game board
	 */
	system("clear");
	for (int y = 0; y < this->m_height; ++y) {
		std::cout << "|";
		for (int x = 0; x < this->m_width; ++x) {
			if (this->m_board[y][x] == '1')
				std::cout << YELLOW;
			else if (this->m_board[y][x] == '2')
				std::cout << RED;
			std::cout << "0" << RESET;
			std::cout << (x < this->m_width - 1 ? " " : "");
		}
		std::cout << "|" << std::endl;
	}
	std::cout << " -------------" << std::endl;
	std::cout << " 1 2 3 4 5 6 7" << std::endl;
}

bool	GameBoard::isColumnFull(const int column) const {
	/*
	 * Check if the column is full
	 *
	 * @param column: the column to check
	 * @return: true if the column is full, false otherwise
	 */
	return (this->m_board[0][column] != '0');
}

void	GameBoard::addPlayerCoin(const Player player, const int column) {
	/*
	 * Add the player coin to the game board
	 *
	 * @param player: the player to add the coin
	 * @param column: the column to add the coin
	 */
	for (int y = this->m_height - 1; y >= 0; --y) {
		if (this->m_board[y][column] == '0') {
			this->m_board[y][column] = player.number;
			break;
		}
	}
}

bool	GameBoard::checkWinDirection(
		const Player player,
		const int x,
		const int y,
		const int dx,
		const int dy) const {
	/*
	 * Check if the player has won in a specific direction
	 *
	 * @param player: the player to check
	 * @param x: the x position to start checking
	 * @param y: the y position to start checking
	 * @param dx: the x direction to check
	 * @param dy: the y direction to check
	 * @return: true if the player has won in the direction, false otherwise
	 */
	for (int i = 0; i < 4; ++i) {
		int x1 = x + i * dx;
		int y1 = y + i * dy;
		if (x1 < 0 || x1 >= this->m_width ||
			y1 < 0 || y1 >= this->m_height ||
			this->m_board[y1][x1] != player.number)
			return (false);
	}
	return (true);
}

bool	GameBoard::checkWin(const Player player) const {
	/*
	 * Check if the player has won
	 *
	 * @param player: the player to check
	 * @return: true if the player has won, false otherwise
	 */
	for (int y = 0; y < this->m_height; ++y) {
		for (int x = 0; x < this->m_width; ++x) {
			if (this->m_board[y][x] == player.number) {
				if (this->checkWinDirection(player, x, y, 1, 0) ||
					this->checkWinDirection(player, x, y, 0, 1) ||
					this->checkWinDirection(player, x, y, 1, 1) ||
					this->checkWinDirection(player, x, y, 1, -1))
					return true;
			}
		}
	}
	return false;
}

bool	GameBoard::checkDraw(void) const {
	/*
	 * Check if the game is a draw
	 *
	 * @return: true if the game is a draw, false otherwise
	 */
	for (int x = 0; x < this->m_width; ++x)
		if (this->m_board[0][x] == '0')
			return (false);
	return (true);
}

void	GameBoard::clear(void) {
	/*
	 * Clear the game board
	 */
	for (int y = 0; y < this->m_height; y++)
		for (int x = 0; x < this->m_width; x++)
			this->m_board[y][x] = '0';
}

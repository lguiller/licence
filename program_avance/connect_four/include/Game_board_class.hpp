#ifndef GAME_BOARD_CLASS_HPP
# define GAME_BOARD_CLASS_HPP

# include <iostream>
# include "Player_class.hpp"

# define WIDTH		7
# define HEIGHT		6

# define YELLOW		"\033[33m"
# define RED		"\033[31m"
# define RESET		"\033[0m"

class	Player;

class	GameBoard {
public:
	GameBoard(void);
	~GameBoard(void);

	void				print(void) const;
	bool				isColumnFull(const int column) const;
	void				addPlayerCoin(const Player player, const int column);
	bool				checkWinDirection(const Player player, const int x, const int y, const int dx, const int dy) const;
	bool				checkWin(const Player player) const;
	bool				checkDraw(void) const;
	void				clear(void);

private:
	static const int	m_width = WIDTH;
	static const int	m_height = HEIGHT;
	char				**m_board;
};

#endif

#ifndef PLAYER_CLASS_HPP
# define PLAYER_CLASS_HPP

# include <iostream>
# include "Game_board_class.hpp"

enum PlayerType {
	PLAYER1 = '1',
	PLAYER2 = '2'
};

class	GameBoard;

class	Player {
public:
	Player(const PlayerType coin);

	bool				dropCoin(GameBoard &board, const int column) const;

	const PlayerType	number;
	const std::string	name;
	const std::string	coin;
};

#endif

#ifndef CONNECT_FOUR_HPP
# define CONNECT_FOUR_HPP

# include <iostream>
//# include "Game_class.hpp"

# include "Game_board_class.hpp"
# include "Player_class.hpp"

# define ESCAPE		-35

int		runTests(void);
int		getUserInput(void);
void	displayMenu(void);

#endif
